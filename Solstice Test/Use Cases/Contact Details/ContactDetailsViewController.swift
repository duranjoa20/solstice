//
//  ContactDetailsViewController.swift
//  Solstice Test
//
//  Created by Joaquin  on 06/04/2019.
//

import UIKit

class ContactDetailsViewController: UIViewController {

    var contactInfo: ContactInformation?
    var mirror: Mirror?
    var fields: [(field: String, value: String)]?

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailsTableView: UITableView!
    @IBOutlet weak var favoriteButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        detailsTableView.dataSource = self
        detailsTableView.delegate = self
        setUpHeaders()

        favoriteButton.image = self.contactInfo?.isFavorite ?? false ?  #imageLiteral(resourceName: "Favorite — True") : #imageLiteral(resourceName: "Favorite — False")
    }


    fileprivate func setUpHeaders() {
        if let url = URL(string: contactInfo?.largeImageURL ?? "") {
            profileImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "User Icon Small"))
        }
        nameLabel.text = contactInfo?.name
        companyLabel.text = contactInfo?.companyName
        fields = self.getFieldsTuple()
//        for (key, value) in fields! {
//            print("this is the key: \(key) FOR THE VALUE \(value)")
//        }
    }

    @IBAction func didTouchFavoriteButton(_ sender: Any) {
        self.contactInfo?.isFavorite = !(self.contactInfo?.isFavorite)!
        favoriteButton.image = self.contactInfo?.isFavorite ?? false ?  #imageLiteral(resourceName: "Favorite — True") : #imageLiteral(resourceName: "Favorite — False")
    }

}


extension ContactDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fields?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let field = fields?[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactDetailsPhonesTableViewCell") as! ContactDetailsPhonesTableViewCell
        if field?.field == "home" || field?.field == "mobile" || field?.field == "work" {
            cell.setupPhoneCell(field: field!)
        } else {
            cell.setupFieldCell(field: field!)
        }
        return cell
    }

}

extension ContactDetailsViewController{
    fileprivate func getFieldsTuple() -> [(field: String, value: String)] {
        var tuples = [(field: String, value: String)]()
        var mirror = Mirror(reflecting: contactInfo!)
        for child in mirror.children {
            if let field = child.label,
                let value = child.value as? String,
                (field == "adreess" || field == "birthdate" || field == "email")  {
                tuples.append((field: field, value: value))
            }
        }
        mirror = Mirror(reflecting: (contactInfo?.phone)!)
        for child in mirror.children {
            if let field = child.label, let value = child.value as? String {
                tuples.append((field: field, value: value))
            }
        }
        if let address = contactInfo?.getAddress() {
            tuples.append((field: "Address", value: address))
        }

        return tuples
    }
}
