//
//  ContactDetailsPhonesTableViewCell.swift
//  Solstice Test
//
//  Created by Joaquin  on 06/04/2019.
//

import UIKit

class ContactDetailsPhonesTableViewCell: UITableViewCell {
    typealias InfoTuple = (field: String, value: String)
    @IBOutlet weak var labelInformation: UILabel!
    @IBOutlet weak var phoneOriginLabel: UILabel!
    @IBOutlet weak var fieldName: UILabel!




    func setupPhoneCell(field: InfoTuple){
        phoneOriginLabel.isHidden = false
        fieldName.text = "PHONE"
        labelInformation.text = field.value
        phoneOriginLabel.text = field.field
    }
    func setupFieldCell(field: InfoTuple){
        phoneOriginLabel.isHidden = true
        fieldName.text = field.field
        labelInformation.text = field.value
    }
}
