//
//  ContactListViewController.swift
//  Solstice Test
//
//  Created by Joaquin  on 06/04/2019.
//

import UIKit

class ContactListViewController: UIViewController {


    var standarContacts: Contacts? = Contacts()
    var favoriteContacts: Contacts? = Contacts()
    var selectedContact: ContactInformation?
    @IBOutlet weak var contactListTableView: UITableView!



    override func viewDidLoad() {
        super.viewDidLoad()
        contactListTableView.delegate = self
        contactListTableView.dataSource = self
        self.viewIfLoaded?.showLoader()
        setupTableView()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        updateModel()
    }

    
    fileprivate func setupTableView(){
        ContactRepository.shared.getContactInformation { (contactInformation) in
            self.standarContacts?.contacts = contactInformation?.contacts?.filter({ (contact) in !contact.isFavorite })
            self.favoriteContacts?.contacts = contactInformation?.contacts?.filter({ (contact) in contact.isFavorite })
            self.viewIfLoaded?.hideLoader()
            self.contactListTableView.reloadData()
        }
    }


    fileprivate func updateModel(){
        guard let selected = selectedContact else { return }

        if selectedContact?.isFavorite ?? false {
            standarContacts?.delete(contact: selected)
            favoriteContacts?.appendAndSort(contact: selected)
        } else {
            favoriteContacts?.delete(contact: selected)
            standarContacts?.appendAndSort(contact: selected)
        }
        self.contactListTableView.reloadData()


    }

}

extension ContactListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        switch section {
        case 0:
            return favoriteContacts?.contacts?.count ?? 0
        case 1:
            return standarContacts?.contacts?.count ?? 0
        default:
            return 0
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactDetailTableViewCell", for: indexPath) as! ContactListTableViewCell
        if (indexPath.section == 0){

            guard let contanctInfo = self.favoriteContacts?.contacts?[indexPath.row] else { return cell }
            cell.setUpFavoriteContact(info: contanctInfo)
        }
        if (indexPath.section == 1){

            guard let contanctInfo = self.standarContacts?.contacts?[indexPath.row] else { return cell }
            cell.setUpStandarContact(info: contanctInfo)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        switch section {
        case 0:
            return "Favorite contacts"
        case 1:
            return "Other contacts"
        default:
            return ""
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            selectedContact = favoriteContacts?.contacts?[indexPath.row]
        case 1:
            selectedContact = standarContacts?.contacts?[indexPath.row]
        default:
            preconditionFailure()
        }
        guard selectedContact != nil else { return }
        let vc = UIStoryboard.SolsticeTest.instantiateContactDetailsViewController()
        vc.contactInfo = selectedContact!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

