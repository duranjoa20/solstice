//
//  ContactDetailTableViewCell.swift
//  Solstice Test
//
//  Created by Joaquin  on 06/04/2019.
//

import UIKit
import SDWebImage

class ContactListTableViewCell: UITableViewCell {

    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var contactNameLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!

    func setUpFavoriteContact(info: ContactInformation) {
        contactNameLabel.text = info.name
        companyNameLabel.text = info.companyName
        if let url = URL(string: info.largeImageURL!) {
            profilePicture.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "User Icon Small"))
        }

    }

    func setUpStandarContact(info: ContactInformation) {
        contactNameLabel.text = info.name
        companyNameLabel.text = info.companyName
        if let url = URL(string: info.largeImageURL!) {
            profilePicture.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "User Icon Small"))
        }



    }

}
