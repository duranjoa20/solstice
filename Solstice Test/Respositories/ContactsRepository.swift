//
//  ContactsRepository.swift
//  Solstice Test
//
//  Created by Joaquin  on 06/04/2019.
//

import Foundation

class ContactRepository {
    var data: [ContactInformation]?
    static let shared = ContactRepository()


    func getContactInformation(_ completition: @escaping (Contacts?) -> Void) {
        Rest.shared.GetContacts { (contactInformation) in
            if let contactsInfo = contactInformation {
                contactsInfo.sortAphabetically()
                contactsInfo.formatPhones()
                completition(contactsInfo)
            }

        }
    }
}
