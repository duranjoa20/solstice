//
//  RestServicePath.swift
//  Solstice Test
//
//  Created by Joaquin  on 06/04/2019.
//

import Foundation

public struct RestServicePath {

    static let host = "https://s3.amazonaws.com/technical-challenge/v3/"

    static let getContacts = "\(host)contacts.json"
    
}
