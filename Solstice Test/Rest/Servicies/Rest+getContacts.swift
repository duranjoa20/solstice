//
//  Rest+getContacts.swift
//  Solstice Test
//
//  Created by Joaquin  on 06/04/2019.
//

import Foundation
import Alamofire

extension Rest {
    public typealias GetContacts = (_ info: Contacts?) -> Void

    public func GetContacts(completion: @escaping GetContacts) {
        self.sessionManager.request(RestServicePath.getContacts, method: .get, parameters: nil, headers: nil).validate().responseJSON { (dataResonse) in
            if dataResonse.result.isSuccess {
                if let data = dataResonse.data{
                    do {
                        let decodedData = try JSONDecoder().decode([ContactInformation]?.self, from: data)
                        let decodedContacts: Contacts? = Contacts()
                        decodedContacts?.contacts = decodedData
                        completion(decodedContacts)
                    } catch {
                        completion(nil)
                    }

                }
            } else {
                completion(nil)
            }

        }
    }

}


