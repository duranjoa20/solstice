//
//  String+Extension.swift
//  Solstice Test
//
//  Created by Joaquin  on 07/04/2019.
//

import Foundation

extension String {
    func formatPhone() -> String? {
        let numbers = self.split(separator: "-")
        if let countryCode = numbers.at(0), let areaCode = numbers.at(1), let identifier = numbers.at(2) {
            return "(\(countryCode)) \(areaCode)-\(identifier)"
        }
        return nil

    }
}
