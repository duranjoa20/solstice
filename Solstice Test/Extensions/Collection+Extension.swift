//
//  Collection+Extension.swift
//  Solstice Test
//
//  Created by Joaquin  on 07/04/2019.
//

import Foundation

extension Collection {
    func at(_ index: Self.Index) -> Element? {
        if startIndex <= index && index < endIndex {
            return self[index]
        } else {
            return nil
        }
    }
    
}
