//
//  UIStoryboard+Extensions.swift
//  Solstice Test
//
//  Created by Joaquin  on 06/04/2019.
//

import Foundation
import UIKit

extension UIStoryboard{

    struct SolsticeTest{

        //MARK: - Load storyboard
        static func mainStoryboard() -> UIStoryboard {
            return UIStoryboard(name: "Main", bundle: nil)
        }


        //MARK: - Main storyboard
        static func instantiateContactDetailsViewController() -> ContactDetailsViewController {
            let storyboard = UIStoryboard.SolsticeTest.mainStoryboard()
            return storyboard.instantiateViewController(withIdentifier: "ContactDetailsViewController") as! ContactDetailsViewController
        }

    }

}

