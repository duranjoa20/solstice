//
//  Adress.swift
//  Solstice Test
//
//  Created by Joaquin  on 06/04/2019.
//

import Foundation

struct Address: Decodable {

    var street: String?
    var city: String?
    var state: String?
    var country: String?
    var zipCode: String?
}
