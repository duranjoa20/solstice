//
//  ContactInformation.swift
//  Solstice Test
//
//  Created by Joaquin  on 06/04/2019.
//

import
Foundation

class ContactInformation: Decodable {


    var name: String
    var id: String
    var companyName: String?
    var isFavorite: Bool
    var smallImageURL: String?
    var largeImageURL: String?
    var emailAddress: String?
    var birthdate: String?
    var phone: Phone?
    var address: Address?


    func getAddress() -> String {
        guard let street = self.address?.street,
            let city = self.address?.city,
            let state = self.address?.state,
            let country = self.address?.country,
            let zipCode = self.address?.zipCode else { return "" }
        return "\(street)\n\(city) \(state), \(zipCode), \(country)"
    }

    func formatPhones() {
        if let homePhone = self.phone?.home?.formatPhone() {
            self.phone?.home = homePhone
        }
        if let workPhone = self.phone?.work?.formatPhone() {
            self.phone?.work = workPhone
        }
        if let mobilePhone = self.phone?.mobile?.formatPhone() {
            self.phone?.mobile = mobilePhone
        }
    }

}
