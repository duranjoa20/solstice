//
//  Phone.swift
//  Solstice Test
//
//  Created by Joaquin  on 06/04/2019.
//

import Foundation

struct Phone: Decodable {
    
    var work: String?
    var home: String?
    var mobile: String?
}
