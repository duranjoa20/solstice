//
//  Contacts.swift
//  Solstice Test
//
//  Created by Joaquin  on 07/04/2019.
//

import Foundation

class Contacts: Decodable {
    var contacts: [ContactInformation]?



    func sortAphabetically() {
        contacts?.sort(by: { (one, two) -> Bool in
            one.name < two.name
        })
    }

    func formatPhones() {
        contacts?.forEach({ (contact) in
            contact.formatPhones()
        })
    }

    func exists(_ contact: ContactInformation) -> Bool{
        if let contacts = self.contacts {
            for cont in contacts {
                if cont.id == contact.id {
                    return true
                }
            }
            return false
        }
        else {
            return false
        }
    }


    func delete(contact: ContactInformation) {
        self.contacts?.removeAll(where: { (cont) -> Bool in
            cont.id == contact.id
        })
    }


    func appendAndSort(contact: ContactInformation){
        if self.exists(contact) { return }
        self.contacts?.append(contact)
        self.sortAphabetically()
    }
}
